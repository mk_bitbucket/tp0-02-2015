package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/6/15.
 */
public class Multiplicacion implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        return (expresion.eval() * expresion.eval());

    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}

