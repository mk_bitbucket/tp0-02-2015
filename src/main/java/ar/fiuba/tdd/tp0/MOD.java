package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class MOD implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        float divisor = expresion.eval();
        float dividendo = expresion.eval();

        return (dividendo % divisor);

    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}
