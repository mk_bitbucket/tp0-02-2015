package ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by mk on 9/6/15.
 */

/*  A partir de una expresion en formato string, y una configuracion determinada
    (que preestablece los evaluables soportados por la calculadora),
    me devuelve la expression lista para usar por la calculadora.
 */

public class RPNExpressionLoader {

    private ArrayList<FabricaDeEvaluable> fabricantesDeEvaluablesSoportados;
    private FabricanteDeEvaluables fabricante;

    public RPNExpressionLoader() {
        this.fabricante = new FabricanteDeEvaluables();
        this.setConfiguracionBasica();
    }

    // soporta sobre numeros flotantes las operaciones: +, -, /, *, MOD, ++, --, **, //
    public void setConfiguracionBasica() {
        fabricantesDeEvaluablesSoportados = new ArrayList<FabricaDeEvaluable>(0);

        fabricantesDeEvaluablesSoportados.add(new FabricaSuma());
        fabricantesDeEvaluablesSoportados.add(new FabricaNumero());
        fabricantesDeEvaluablesSoportados.add(new FabricaResta());
        fabricantesDeEvaluablesSoportados.add(new FabricaDivision());
        fabricantesDeEvaluablesSoportados.add(new FabricaMultiplicacion());
        fabricantesDeEvaluablesSoportados.add(new FabricaMOD());
        fabricantesDeEvaluablesSoportados.add(new FabricaSumaMultiple());
        fabricantesDeEvaluablesSoportados.add(new FabricaMultiplicacionMultiple());
        fabricantesDeEvaluablesSoportados.add(new FabricaRestaMultiple());
        fabricantesDeEvaluablesSoportados.add(new FabricaDivisionMultiple());

    }

    public RPNExpresion loadExpression(String expression) {

        RPNExpresion expresion = new RPNExpresion();

        if (expression != null) { // for test: nullExpression
            String[] expressionEvaluables = expression.split(" ");
            for (int index = 0; index < expressionEvaluables.length; index++) {

                this.loadEvaluable(expressionEvaluables[index], expresion);
            }
        }

        return expresion;
    }

    private void loadEvaluable(String potencialEvaluable, RPNExpresion expresion) {

        Evaluable evaluableNuevo = new EvaluableNull();
        Iterator<FabricaDeEvaluable> iteradorEvaluablesSoportados = this.fabricantesDeEvaluablesSoportados.iterator();

        while (evaluableNuevo.soyEvaluableNull() && iteradorEvaluablesSoportados.hasNext()) {

            FabricaDeEvaluable fabricaDeEvaluable = iteradorEvaluablesSoportados.next();
            evaluableNuevo = this.fabricante.crearEvaluableConFabrica(potencialEvaluable,fabricaDeEvaluable);

        }

        expresion.agregar(evaluableNuevo);
    }



}
