package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */

public interface FabricaDeEvaluable {

    public Evaluable crear(String valor);

    public Boolean permiteAlEvaluable(String potencialEvaluable);
}
