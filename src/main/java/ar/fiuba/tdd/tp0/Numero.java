package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */
public class Numero implements Evaluable {

    private float valor;

    public Numero() {

    }

    /*public void setValor(float valor) {
        this.valor=valor;
    }*/

    public Numero(float valor){
        this.valor=valor;
    }

    public float getValor() {
        return this.valor;
    }

    public float evaluarEn(RPNExpresion expresion) {
        return this.getValor();
    }

    public boolean soyEvaluableNull() {
        return false;
    }


}
