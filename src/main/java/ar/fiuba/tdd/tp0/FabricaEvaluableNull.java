package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/10/15.
 */
public class FabricaEvaluableNull implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new EvaluableNull();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "NULL".equals(potencialEvaluable);
    }
}
