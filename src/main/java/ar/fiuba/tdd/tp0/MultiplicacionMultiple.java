package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class MultiplicacionMultiple implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        Evaluable resultado = new EvaluableNull();
        float valorParcial = 1;
        while (expresion.isNotEmpty()) {
            valorParcial *= expresion.eval();
            resultado = new Numero(valorParcial);
        }
        return resultado.getValor();
    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}
