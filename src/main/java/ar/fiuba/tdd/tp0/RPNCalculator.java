package ar.fiuba.tdd.tp0;

public class RPNCalculator {

    private RPNExpresion expresion;
    private RPNExpressionLoader rpnExpressionLoader;

    public RPNCalculator() {

        rpnExpressionLoader = new RPNExpressionLoader();
    }

    public float eval(String expression) {

        this.expresion = this.rpnExpressionLoader.loadExpression(expression);
        return this.expresion.eval();
    }

}
