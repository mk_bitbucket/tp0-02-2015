package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */
public class FabricaSuma implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new Suma();
    }


    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "+".equals(potencialEvaluable);
    }
}
