package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class FabricaMultiplicacion implements FabricaDeEvaluable {


    public Evaluable crear(String valor) {

        return new Multiplicacion();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "*".equals(potencialEvaluable);
    }
}
