package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/6/15.
 */
public class Resta implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        // es necesario seguir este orden
        float segundoValor = expresion.eval();
        float primerValor = expresion.eval();

        return (primerValor - segundoValor);
    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}
