package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class FabricaDivisionMultiple implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new DivisionMultiple();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {

        return "//".equals(potencialEvaluable);
    }

}
