package ar.fiuba.tdd.tp0;

import java.beans.Expression;
import java.util.ArrayList;

/**
 * Created by mk on 9/7/15.
 */
public class RestaMultiple implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        Evaluable resultado = new Numero(expresion.eval());
        while (expresion.isNotEmpty()) {

            resultado = new Numero(this.evaluarEn(expresion) - resultado.getValor());
        }
        return resultado.getValor();
    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}
