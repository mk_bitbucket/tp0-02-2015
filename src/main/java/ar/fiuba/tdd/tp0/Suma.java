package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */
public class Suma implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        return (expresion.eval() + expresion.eval());

    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }

}
