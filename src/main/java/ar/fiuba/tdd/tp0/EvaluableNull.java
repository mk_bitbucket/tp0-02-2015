package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */
public class EvaluableNull implements Evaluable {

    public float evaluarEn(RPNExpresion expresion) {

        throw new IllegalArgumentException();
    }

    public boolean soyEvaluableNull() {
        return true;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }


}
