package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class FabricaDivision implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new Division();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "/".equals(potencialEvaluable);
    }
}