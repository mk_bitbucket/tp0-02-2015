package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/6/15.
 */

public class FabricaNumero implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new Numero(Float.parseFloat(valor));
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {

        String soloNumerosRegularExpression = "[-+]?(\\d*[.])?\\d+";
        return potencialEvaluable.matches(soloNumerosRegularExpression);

    }
}
