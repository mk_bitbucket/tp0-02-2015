package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class Division implements Evaluable {

    // no valida el caso de que el divisor sea 0 !!!
    public float evaluarEn(RPNExpresion expresion) {

        float divisor = expresion.eval();
        float dividendo = expresion.eval();

        return (dividendo / divisor);

    }

    public boolean soyEvaluableNull() {
        return false;
    }

    public float getValor() {

        throw new IllegalArgumentException();

    }


}
