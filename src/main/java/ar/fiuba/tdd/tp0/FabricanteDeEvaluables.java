package ar.fiuba.tdd.tp0;

import java.util.HashMap;

/**
 * Created by mk on 9/10/15.
 */
public class FabricanteDeEvaluables {

    private  HashMap<Boolean, FabricaDeEvaluable> validadorDeFabricaAOperar = new HashMap<Boolean, FabricaDeEvaluable>(0);

    public FabricanteDeEvaluables() {
        validadorDeFabricaAOperar.put(Boolean.FALSE, new FabricaEvaluableNull());
    }

    public Evaluable crearEvaluableConFabrica(String potencialEvaluable, FabricaDeEvaluable fabricaDeEvaluable) {

        validadorDeFabricaAOperar.put(Boolean.TRUE, fabricaDeEvaluable);
        FabricaDeEvaluable fabricaAOperar = validadorDeFabricaAOperar.get(fabricaDeEvaluable.permiteAlEvaluable(potencialEvaluable));

        return fabricaAOperar.crear(potencialEvaluable);
    }
}
