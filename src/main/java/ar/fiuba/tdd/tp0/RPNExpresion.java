package ar.fiuba.tdd.tp0;

import java.util.ArrayList;

/**
 * Created by mk on 9/4/15.
 */
public class RPNExpresion {

    private ArrayList<Evaluable> evaluables;

    public RPNExpresion() {
        evaluables = new ArrayList<Evaluable>(0);
        this.agregar(new EvaluableNull()); // for test: incompleteExpression
    }

    public boolean isNotEmpty() {
        return (this.evaluables.size() > 1);
    }

    public void agregar(Evaluable evaluable) {

        this.evaluables.add(evaluable);
    }

    public Evaluable pop() {

        int lastEvaluableIndex = this.evaluables.size() - 1;
        Evaluable evaluable = this.evaluables.get(lastEvaluableIndex);
        this.evaluables.remove(lastEvaluableIndex);

        return evaluable;
    }

    public float eval() {

        return this.pop().evaluarEn(this);
    }


}
