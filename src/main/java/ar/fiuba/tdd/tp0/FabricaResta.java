package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/6/15.
 */

public class FabricaResta implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new Resta();
    }


    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "-".equals(potencialEvaluable);
    }
}
