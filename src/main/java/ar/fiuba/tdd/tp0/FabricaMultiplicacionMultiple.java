package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class FabricaMultiplicacionMultiple implements FabricaDeEvaluable {

    public Evaluable crear(String simbolo) {

        return new MultiplicacionMultiple();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {
        return "**".equals(potencialEvaluable);
    }
}
