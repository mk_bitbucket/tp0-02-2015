package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/7/15.
 */
public class FabricaMOD implements FabricaDeEvaluable {

    public Evaluable crear(String valor) {

        return new MOD();
    }

    public Boolean permiteAlEvaluable(String potencialEvaluable) {

        return "MOD".equals(potencialEvaluable);
    }
}