package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/4/15.
 */
public interface Evaluable {

    public float evaluarEn(RPNExpresion expresion);

    public boolean soyEvaluableNull();

    public float getValor();
}
