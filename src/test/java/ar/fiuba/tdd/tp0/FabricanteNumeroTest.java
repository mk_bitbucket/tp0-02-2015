package ar.fiuba.tdd.tp0;

/**
 * Created by mk on 9/6/15.
 */
import org.junit.Test;

import static org.junit.Assert.assertEquals;



public class FabricanteNumeroTest {

    private static final double DELTA = 0.00001;
    private final FabricaNumero fabricanteNumero = new FabricaNumero();


    @Test
    public void crearUnNumeroEnteroPositivo() {
        assertEquals(5, ((Numero) fabricanteNumero.crear("5") ).getValor() , DELTA);
    }

    @Test
    public void crearUnNumeroEnteroNegativo() {
        assertEquals(-4, ((Numero) fabricanteNumero.crear("-4") ).getValor() , DELTA);
    }

    @Test
    public void crearUnNumeroConComa() {
        assertEquals(5.09, ((Numero) fabricanteNumero.crear("5.09") ).getValor() , DELTA);
    }

    public void crearUnNumeroNegativoConComa() {
        assertEquals(-0.4, ((Numero) fabricanteNumero.crear("-0.4")).getValor(), DELTA);
    }


    @Test(expected = IllegalArgumentException.class)
    public void simboloMasMasEsErroneo() {
        ((EvaluableNull) fabricanteNumero.crear("++") ).getValor();
    }

    @Test(expected = IllegalArgumentException.class)
    public void simboloConLetrasEsErroneo() {
        ((EvaluableNull) fabricanteNumero.crear("23fs") ).getValor();
    }

    @Test(expected = IllegalArgumentException.class)
    public void numeroConComaMalFormado() {
        ((EvaluableNull) fabricanteNumero.crear(".34.") ).getValor();
    }
}